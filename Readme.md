# Documentation des Modifications Proxmox pour l'Intégration de DRBD

## 1. Modifications du fork

## pve-docs:

**Fonctionnement :**
- Script qui recupère tous les fichiers adoc présent pour les compiler. 

**Modifications :**
- `pvedrbd.adoc` : Ajout de la documentation relative a DRBD, accessible depuis l'UI de promox.

## 2. Instructions d'installation

Ce paquet est essentiel pour le fonctionnement de [pve-drbd](https://gitea.evolix.org/evolix/pve-drbd.git). Il est donc inutile de l'installer seul.

1. Télécharger les sources du paquet ([pve-docs](https://gitea.evolix.org/evolix/pve-docs.git))
2. Compiler et installer avec `make dinstall` ou `gbp buildpackage` (dans la branch debian)
